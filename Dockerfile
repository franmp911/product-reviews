FROM openjdk:8
ADD target/product-reviews.jar product-reviews.jar
EXPOSE 8666
ENTRYPOINT ["java", "-jar", "product-reviews.jar"]
