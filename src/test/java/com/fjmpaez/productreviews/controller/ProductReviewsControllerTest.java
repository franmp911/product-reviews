package com.fjmpaez.productreviews.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.ws.rs.core.MediaType;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fjmpaez.exceptionslib.util.Utils;
import com.fjmpaez.productreviews.ProductReviewsApplication;
import com.fjmpaez.productreviews.model.ReviewDB;
import com.fjmpaez.productreviews.model.ReviewRepository;
import com.fjmpaez.productreviews.pojo.Review;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ProductReviewsApplication.class)
public class ProductReviewsControllerTest {

    private static final String GET_PRODUCTS_REVIEWS_URL = "http://localhost:8666/v1.0/review";
    private static final String GET_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review/{product_id}";
    private static final String POST_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review";
    private static final String PUT_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review/{product_id}";
    private static final String DELETE_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review/{product_id}";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private MockMvc mockMvc;

    @Autowired
    private ProductReviewsController productReviewsController;

    @MockBean
    private Utils utils;

    @Autowired
    private ReviewRepository reviewRepository;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(productReviewsController).build();
        when(utils.getCurrentCorrelationId()).thenReturn("correlation1234test");
    }

    @After
    public void clean() {
        reviewRepository.deleteAll();
    }

    @Test
    public void getProductsReviewsSize2() throws Exception {

        reviewRepository.save(new ReviewDB("C77154", 2, 10));
        reviewRepository.save(new ReviewDB("M20324", 9, 100));

        mockMvc.perform(get(GET_PRODUCTS_REVIEWS_URL)).andExpect(status().isOk())
                .andExpect(jsonPath("$.reviews", Matchers.hasSize(2)))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test
    public void getProductsReviewsEmpty() throws Exception {

        mockMvc.perform(get(GET_PRODUCTS_REVIEWS_URL)).andExpect(status().isOk())
                .andExpect(jsonPath("$.reviews", Matchers.hasSize(0)))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test
    public void getProductReview() throws Exception {

        reviewRepository.save(new ReviewDB("C77154", 2, 10));

        mockMvc.perform(get(GET_PRODUCT_REVIEW_URL, "C77154")).andExpect(status().isOk())
                .andExpect(jsonPath("$.product_id", Matchers.is("C77154")))
                .andExpect(jsonPath("$.reviews_avg_score", Matchers.is(2)))
                .andExpect(jsonPath("$.reviews_count", Matchers.is(10)))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test(expected = Exception.class)
    public void getProductReviewNonExists() throws Exception {

        mockMvc.perform(get(GET_PRODUCT_REVIEW_URL, "C77154")).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error_code", Matchers.is("001003")))
                .andExpect(jsonPath("$.description", Matchers.is("review not found")))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test
    public void createProductReview() throws Exception {

        Review reviewRequest = new Review("C77154", 2, 10);

        mockMvc.perform(post(POST_PRODUCT_REVIEW_URL)
                .content(MAPPER.writer().writeValueAsString(reviewRequest))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(jsonPath("$.product_id", Matchers.is("C77154")))
                .andExpect(jsonPath("$.reviews_avg_score", Matchers.is(2)))
                .andExpect(jsonPath("$.reviews_count", Matchers.is(10)))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test
    public void createProductReviewAlreadyExists() throws Exception {

        reviewRepository.save(new ReviewDB("C77154", 1, 1));

        Review reviewRequest = new Review("C77154", 2, 10);

        mockMvc.perform(post(POST_PRODUCT_REVIEW_URL)
                .content(MAPPER.writer().writeValueAsString(reviewRequest))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(jsonPath("$.product_id", Matchers.is("C77154")))
                .andExpect(jsonPath("$.reviews_avg_score", Matchers.is(2)))
                .andExpect(jsonPath("$.reviews_count", Matchers.is(10)))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test
    public void updateProductReview() throws Exception {

        reviewRepository.save(new ReviewDB("C77154", 1, 1));

        Review reviewRequest = new Review("C77154", 2, 10);

        mockMvc.perform(put(PUT_PRODUCT_REVIEW_URL, "C77154")
                .content(MAPPER.writer().writeValueAsString(reviewRequest))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.product_id", Matchers.is("C77154")))
                .andExpect(jsonPath("$.reviews_avg_score", Matchers.is(2)))
                .andExpect(jsonPath("$.reviews_count", Matchers.is(10)))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test(expected = Exception.class)
    public void updateProductReviewNonExists() throws Exception {

        Review reviewRequest = new Review("C77154", 2, 10);

        mockMvc.perform(put(PUT_PRODUCT_REVIEW_URL, "C77154")
                .content(MAPPER.writer().writeValueAsString(reviewRequest))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error_code", Matchers.is("001003")))
                .andExpect(jsonPath("$.description", Matchers.is("review not found")))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test(expected = Exception.class)
    public void updateProductReviewWrongProductId() throws Exception {

        Review reviewRequest = new Review("C77154", 2, 10);

        mockMvc.perform(put(PUT_PRODUCT_REVIEW_URL, "C77155")
                .content(MAPPER.writer().writeValueAsString(reviewRequest))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error_code", Matchers.is("001002")))
                .andExpect(jsonPath("$.description", Matchers.is("invalid parameter: product_id")))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    @Test
    public void deleteProductReview() throws Exception {

        reviewRepository.save(new ReviewDB("C77154", 1, 1));

        Review reviewRequest = new Review("C77154", 2, 10);

        mockMvc.perform(delete(DELETE_PRODUCT_REVIEW_URL, "C77154")
                .content(MAPPER.writer().writeValueAsString(reviewRequest))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
    }

    @Test(expected = Exception.class)
    public void deleteProductReviewNonExists() throws Exception {

        Review reviewRequest = new Review("C77154", 2, 10);

        mockMvc.perform(delete(DELETE_PRODUCT_REVIEW_URL, "C77154")
                .content(MAPPER.writer().writeValueAsString(reviewRequest))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error_code", Matchers.is("001003")))
                .andExpect(jsonPath("$.description", Matchers.is("review not found")))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

}
