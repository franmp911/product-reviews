package com.fjmpaez.productreviews.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fjmpaez.exceptionslib.DefinedErrorException;
import com.fjmpaez.exceptionslib.util.Utils;
import com.fjmpaez.productreviews.exception.ErrorEnum;
import com.fjmpaez.productreviews.model.ReviewDB;
import com.fjmpaez.productreviews.model.ReviewModelServiceImpl;
import com.fjmpaez.productreviews.model.ReviewRepository;
import com.fjmpaez.productreviews.pojo.ProductsReviewsResponse;
import com.fjmpaez.productreviews.pojo.Review;
import com.fjmpaez.productreviews.pojo.ReviewResponse;

@RunWith(MockitoJUnitRunner.class)
public class ReviewModelServiceImplTest {

    @InjectMocks
    private ReviewModelServiceImpl reviewModelServiceImpl;

    @Mock
    private Utils utils;

    @Mock
    private ReviewRepository reviewRepository;

    @Before
    public void setUp() throws Exception {
        when(utils.getCurrentCorrelationId()).thenReturn("correlation1234test");
    }

    @Test
    public void getProductsReviews() {

        when(reviewRepository.findAll()).thenReturn(getMockReviewDBList(3));

        ProductsReviewsResponse result = reviewModelServiceImpl.getProductsReviews();

        assertThat(result.getCorrelationId(), notNullValue());
        assertThat(result.getReviews().size(), is(3));

        verify(reviewRepository).findAll();
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void getProductsReviewsEmptyList() {

        when(reviewRepository.findAll()).thenReturn(getMockReviewDBList(0));

        ProductsReviewsResponse result = reviewModelServiceImpl.getProductsReviews();

        assertThat(result.getCorrelationId(), notNullValue());
        assertThat(result.getReviews(), notNullValue());
        assertThat(result.getReviews().size(), is(0));

        verify(reviewRepository).findAll();
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void getProductReview() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString()))
                .thenReturn(Optional.of(getMockReviewDB(1)));

        ReviewResponse result = reviewModelServiceImpl.getProductReview("1");

        assertThat(result.getCorrelationId(), notNullValue());
        assertThat(result.getProductId(), is("1"));

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void getProductReviewNotFound() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString())).thenReturn(Optional.empty());

        try {
            reviewModelServiceImpl.getProductReview("1");
        } catch (DefinedErrorException e) {
            assertThat(e.getErrorEnum(), is(ErrorEnum.NOT_FOUND));
        }

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void saveReview() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString()))
                .thenReturn(Optional.of(getMockReviewDB(1)));

        when(reviewRepository.save(any(ReviewDB.class))).thenReturn(getMockReviewDB(1));

        Review review = new Review("1", 3, 4);

        ReviewResponse result = reviewModelServiceImpl.saveReview(review);

        assertThat(result.getCorrelationId(), notNullValue());
        assertThat(result.getProductId(), is("1"));

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verify(reviewRepository).save(any(ReviewDB.class));
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void saveReviewAlreadyExists() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString())).thenReturn(Optional.empty());

        when(reviewRepository.save(any(ReviewDB.class))).thenReturn(getMockReviewDB(1));

        Review review = new Review("1", 3, 4);

        ReviewResponse result = reviewModelServiceImpl.saveReview(review);

        assertThat(result.getCorrelationId(), notNullValue());
        assertThat(result.getProductId(), is("1"));

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verify(reviewRepository).save(any(ReviewDB.class));
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void updateReview() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString()))
                .thenReturn(Optional.of(getMockReviewDB(1)));

        when(reviewRepository.save(any(ReviewDB.class))).thenReturn(getMockReviewDB("1", 30, 40));

        Review review = new Review("1", 30, 40);

        ReviewResponse result = reviewModelServiceImpl.updateReview(review);

        assertThat(result.getCorrelationId(), notNullValue());
        assertThat(result.getProductId(), is("1"));
        assertThat(result.getAvgScore(), is(30));
        assertThat(result.getCount(), is(40));

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verify(reviewRepository).save(any(ReviewDB.class));
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void updateReviewNonExists() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString())).thenReturn(Optional.empty());

        try {

            Review review = new Review("1", 30, 40);
            reviewModelServiceImpl.updateReview(review);

        } catch (DefinedErrorException e) {
            assertThat(e.getErrorEnum(), is(ErrorEnum.NOT_FOUND));
        }

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void removeReview() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString()))
                .thenReturn(Optional.of(getMockReviewDB(1)));

        doNothing().when(reviewRepository).delete(any(ReviewDB.class));

        reviewModelServiceImpl.removeReview("1");

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verify(reviewRepository).delete(any(ReviewDB.class));
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void removeReviewNonExists() {

        when(reviewRepository.findByProductIdIgnoreCase(anyString())).thenReturn(Optional.empty());

        try {

            reviewModelServiceImpl.removeReview("1");

        } catch (DefinedErrorException e) {
            assertThat(e.getErrorEnum(), is(ErrorEnum.NOT_FOUND));
        }

        verify(reviewRepository).findByProductIdIgnoreCase(anyString());
        verifyNoMoreInteractions(reviewRepository);
    }

    private List<ReviewDB> getMockReviewDBList(int size) {

        List<ReviewDB> list = new ArrayList<>();

        while (list.size() < size) {
            list.add(getMockReviewDB(list.size()));
        }

        return list;
    }

    private ReviewDB getMockReviewDB(int productId) {

        return new ReviewDB(productId, String.valueOf(productId), productId + 2, productId + 3,
                null);
    }

    private ReviewDB getMockReviewDB(String productId, Integer avgScore, Integer count) {

        return new ReviewDB(1, productId, avgScore, count, null);
    }

}
