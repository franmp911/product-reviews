package com.fjmpaez.productreviews.util;


import java.sql.Types;

import org.hibernate.dialect.HSQLDialect;

public class MyHSQLDBDialect extends HSQLDialect {

    public MyHSQLDBDialect() {
        super();
        registerColumnType(Types.CLOB, Long.MAX_VALUE, "clob");
        registerColumnType(Types.VARCHAR, Long.MAX_VALUE, "longvarchar");
    }

}
