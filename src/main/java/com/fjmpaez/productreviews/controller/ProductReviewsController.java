package com.fjmpaez.productreviews.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fjmpaez.exceptionslib.DefinedErrorException;
import com.fjmpaez.productreviews.exception.ErrorEnum;
import com.fjmpaez.productreviews.model.ReviewModelService;
import com.fjmpaez.productreviews.pojo.ProductsReviewsResponse;
import com.fjmpaez.productreviews.pojo.Review;
import com.fjmpaez.productreviews.pojo.ReviewResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/v1.0/review")
public class ProductReviewsController {

    @Autowired
    private ReviewModelService service;

    @GetMapping(path = "")
    @ResponseStatus(value = HttpStatus.OK)
    public ProductsReviewsResponse getProductsReviews() {

        log.info("received request to getProductsReviews");

        return service.getProductsReviews();
    }

    @PostMapping(path = "")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ReviewResponse createProductReview(@Valid @RequestBody Review reviewRequest) {

        log.info("received request to createProductReview - {}", reviewRequest);

        return service.saveReview(reviewRequest);
    }

    @GetMapping(path = "/{product_id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ReviewResponse getProductReview(@NotBlank @PathVariable("product_id") String productId) {

        log.info("received request to getProductReview - product_id: {}", productId);

        return service.getProductReview(productId);
    }

    @PutMapping(path = "/{product_id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ReviewResponse updateProductReview(
            @NotBlank @PathVariable("product_id") String productId,
            @Valid @RequestBody Review reviewRequest) {

        log.info("received request to updateProductReview - product_id: {} - {}", productId,
                reviewRequest);

        if (!productId.equalsIgnoreCase(reviewRequest.getProductId())) {
            throw new DefinedErrorException(ErrorEnum.INVALID_REQUEST, "product_id");
        }

        return service.updateReview(reviewRequest);
    }

    @DeleteMapping(path = "/{product_id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteProductReview(@NotBlank @PathVariable("product_id") String productId) {

        log.info("received request to deleteProductReview - product_id: {}", productId);

        service.removeReview(productId);
    }

}
