package com.fjmpaez.productreviews.model;

import com.fjmpaez.productreviews.pojo.ProductsReviewsResponse;
import com.fjmpaez.productreviews.pojo.Review;
import com.fjmpaez.productreviews.pojo.ReviewResponse;

public interface ReviewModelService {

    ProductsReviewsResponse getProductsReviews();

    ReviewResponse getProductReview(String productId);

    ReviewResponse saveReview(Review review);

    ReviewResponse updateReview(Review review);

    void removeReview(String productId);

}
