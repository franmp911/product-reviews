package com.fjmpaez.productreviews.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fjmpaez.exceptionslib.DefinedErrorException;
import com.fjmpaez.exceptionslib.util.Utils;
import com.fjmpaez.productreviews.exception.ErrorEnum;
import com.fjmpaez.productreviews.pojo.ProductsReviewsResponse;
import com.fjmpaez.productreviews.pojo.Review;
import com.fjmpaez.productreviews.pojo.ReviewResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReviewModelServiceImpl implements ReviewModelService {

    @Autowired
    private Utils utils;

    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public ProductsReviewsResponse getProductsReviews() {

        List<Review> reviews = reviewRepository.findAll().stream()
                .map(item -> new Review(item.getProductId(), item.getAvgScore(), item.getCount()))
                .collect(Collectors.toList());

        return new ProductsReviewsResponse(reviews, utils.getCurrentCorrelationId());
    }

    @Override
    public ReviewResponse getProductReview(String productId) {

        ReviewDB reviewDB = getReviewDB(productId);

        return new ReviewResponse(reviewDB.getProductId(), reviewDB.getAvgScore(),
                reviewDB.getCount(), utils.getCurrentCorrelationId());
    }

    @Override
    public ReviewResponse saveReview(Review review) {

        ReviewDB reviewDB = new ReviewDB(review.getProductId(), review.getAvgScore(),
                review.getCount());
        reviewDB.setLastUpdateTimestamp(new Timestamp(new Date().getTime()));

        reviewRepository.findByProductIdIgnoreCase(review.getProductId())
                .ifPresent(item -> reviewDB.setId(item.getId()));

        ReviewDB savedReviewDB = reviewRepository.save(reviewDB);

        log.debug("obj saved {}", savedReviewDB);

        return new ReviewResponse(savedReviewDB.getProductId(), savedReviewDB.getAvgScore(),
                savedReviewDB.getCount(), utils.getCurrentCorrelationId());
    }

    @Override
    public ReviewResponse updateReview(Review review) {

        ReviewDB reviewDB = getReviewDB(review.getProductId());
        reviewDB.setAvgScore(review.getAvgScore());
        reviewDB.setCount(review.getCount());
        reviewDB.setLastUpdateTimestamp(new Timestamp(new Date().getTime()));

        ReviewDB savedReviewDB = reviewRepository.save(reviewDB);

        log.debug("obj saved {}", savedReviewDB);

        return new ReviewResponse(savedReviewDB.getProductId(), savedReviewDB.getAvgScore(),
                savedReviewDB.getCount(), utils.getCurrentCorrelationId());
    }

    @Override
    public void removeReview(String productId) {
        reviewRepository.delete(getReviewDB(productId));
    }

    private ReviewDB getReviewDB(String productId) {

        return reviewRepository.findByProductIdIgnoreCase(productId)
                .orElseThrow(() -> new DefinedErrorException(ErrorEnum.NOT_FOUND));
    }

}
