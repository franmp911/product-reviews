package com.fjmpaez.productreviews.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends CrudRepository<ReviewDB, Long> {

    List<ReviewDB> findAll();

    Optional<ReviewDB> findById(Long id);

    Optional<ReviewDB> findByProductIdIgnoreCase(String productId);

}
