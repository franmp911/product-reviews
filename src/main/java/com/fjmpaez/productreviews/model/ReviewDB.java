package com.fjmpaez.productreviews.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "reviews")
public class ReviewDB implements Serializable {

    private static final long serialVersionUID = -7321202773360631984L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "review_avg_score")
    private Integer avgScore;

    @Column(name = "review_count")
    private Integer count;

    @Column(name = "last_update_timestamp")
    private Timestamp lastUpdateTimestamp;

    public ReviewDB(String productId, Integer avgScore, Integer count) {
        super();
        this.productId = productId;
        this.avgScore = avgScore;
        this.count = count;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
