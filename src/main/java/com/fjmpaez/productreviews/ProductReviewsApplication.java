package com.fjmpaez.productreviews;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = { "com.fjmpaez" })
public class ProductReviewsApplication {

    public static void main(String[] args) {
        configureSystem();
        SpringApplication.run(ProductReviewsApplication.class, args);
    }

    private static void configureSystem() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
