CREATE TABLE IF NOT EXISTS reviews (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    product_id VARCHAR(64) NOT NULL,
    review_avg_score INT NOT NULL DEFAULT 0,
    review_count INT NOT NULL DEFAULT 0,
    last_update_timestamp TIMESTAMP NOT NULL DEFAULT now(),
  PRIMARY KEY (id),
  UNIQUE KEY product_id_index (product_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO reviews (product_id, review_avg_score, review_count) VALUES ('M20324', 9, 100);
INSERT INTO reviews (product_id, review_avg_score, review_count) VALUES ('AC7836', 1, 1);
INSERT INTO reviews (product_id, review_avg_score, review_count) VALUES ('C77154', 2, 10);
INSERT INTO reviews (product_id, review_avg_score, review_count) VALUES ('BB5476', 10, 5);

